import java.util.Scanner

/*
    * NAME: Erik Vasilyan
    * DATE: 2023-02-03
    * TITLE: Carpinteria
 */

class Taulell(preuUnitari: Int, llargada: Int, amplada: Int) {
    var preu: Int = 0

    init {
        val area = llargada * amplada
        preu = preuUnitari * area
    }
}

class Llisto(preuUnitari: Int, llargada: Int) {
    var preu: Int = 0

    init {
        preu = preuUnitari * llargada
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    var preuTotal = 0

    val elements = scanner.nextInt()

    for (i in 1 .. elements) {
        val elementNombre = scanner.next().uppercase()
        val elementPreuUnitari = scanner.nextInt()
        val elementLlargada = scanner.nextInt()

        if (elementNombre == "TAULELL") {
            val elementAmplada = scanner.nextInt()
            val taulell = Taulell(elementPreuUnitari, elementLlargada, elementAmplada)
            preuTotal += taulell.preu
        }
        else if (elementNombre == "LLISTÓ") {
            val llisto = Llisto(elementPreuUnitari, elementLlargada)
            preuTotal += llisto.preu
        }
    }
    println("El preu total és: $preuTotal€")
}