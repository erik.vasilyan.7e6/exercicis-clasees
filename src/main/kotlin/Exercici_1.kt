/*
    * NAME: Erik Vasilyan
    * DATE: 2023-02-02
    * TITLE: Pastisseria
 */

class Pasta (var nom: String, var preu: Double, var pes: Int, var calories: Int)

fun main() {
    val croissant = Pasta("Croissant", 2.0, 50, 280)
    println("Nom: ${croissant.nom}")
    println("Preu: ${croissant.preu}€")
    println("Pes: ${croissant.pes}g")
    println("Calories: ${croissant.calories}kcal\n")

    val ensaimada = Pasta("Ensaimada", 10.0, 600, 2490)
    println("Nom: ${ensaimada.nom}")
    println("Preu: ${ensaimada.preu}€")
    println("Pes: ${ensaimada.pes}g")
    println("Calories: ${ensaimada.calories}kcal\n")

    val donut = Pasta("Donut", 1.0, 52, 209)
    println("Nom: ${donut.nom}")
    println("Preu: ${donut.preu}€")
    println("Pes: ${donut.pes}g")
    println("Calories: ${donut.calories}kcal")
}