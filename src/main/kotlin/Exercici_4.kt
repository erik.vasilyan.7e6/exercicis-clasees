/*
    * NAME: Erik Vasilyan
    * DATE: 2023-02-03
    * TITLE: Làmpada d'Ali Baba
 */

//definició dels colors que farem servir, agafant els backgrounds ANSI, i establint-los com a constants globals
const val ANSI_RESET = "\u001B[0m"
const val ANSI_BLACK = "\u001B[40m"
const val ANSI_RED = "\u001B[41m"
const val ANSI_GREEN = "\u001B[42m"
const val ANSI_YELLOW = "\u001B[43m"
const val ANSI_BLUE = "\u001B[44m"
const val ANSI_PURPLE = "\u001B[45m"
const val ANSI_CYAN = "\u001B[46m"
const val ANSI_WHITE = "\u001B[47m"

class Lampada(var nom: String) {
    val colors = arrayOf(ANSI_BLACK, ANSI_WHITE, ANSI_RED, ANSI_GREEN, ANSI_YELLOW, ANSI_BLUE, ANSI_PURPLE, ANSI_CYAN)
    var color = 1
    var intensitat = 1
    private var offColor = color
    var lampOn = false

    fun encendre() {
        lampOn = true
        if (color == 0) {
            color = offColor
            intensitat = 1
        }
    }

    fun apagar() {
        lampOn = false
        if (color > 0) {
            offColor = color
            color = resetLamp()
            intensitat = resetLamp()
        }
    }

    fun canviarColor() {
        color++
    }

    fun canviarIntensitat() {
        intensitat++
    }
}

fun main(){

    val listOfInstructions = listOf(
        listOf("TURN ON", "CHANGE COLOR", "CHANGE COLOR", "CHANGE COLOR", "INTENSITY", "INTENSITY", "INTENSITY", "INTENSITY"),
        listOf("TURN ON", "CHANGE COLOR", "CHANGE COLOR", "INTENSITY", "INTENSITY", "INTENSITY", "INTENSITY", "TURN OFF", "CHANGE COLOR", "TURN ON", "CHANGE COLOR", "INTENSITY", "INTENSITY", "INTENSITY", "INTENSITY")
    )

    val lampadaMenjador = Lampada("Menjador")
    val lampadaHabitacio = Lampada("Habitació")
    val lampadaList = listOf(lampadaMenjador, lampadaHabitacio)

    for ((lampadaIndex, instructions) in listOfInstructions.withIndex()) {
        for (instruction in instructions) {
            val lampada = lampadaList[lampadaIndex]

            when(instruction) {
                "TURN ON" -> { lampada.encendre() }
                "TURN OFF" -> { lampada.apagar() }
                "CHANGE COLOR" -> {
                    if (lampada.lampOn) {
                        if (lampada.color <= lampada.colors.size-2) lampada.canviarColor()
                        else if(lampada.color == lampada.colors.size-2) lampada.color = resetColor()
                    }
                }
                "INTENSITY" -> {
                    if (lampada.lampOn) {
                        lampada.canviarIntensitat()
                        if (lampada.intensitat == 6) lampada.intensitat = resetIntensity()
                    }
                }
            }
            println("${lampada.nom} - Color: ${lampada.colors[lampada.color]}    $ANSI_RESET - intensitat ${lampada.intensitat}")
        }
    }
}

fun resetColor(): Int {
    return 1
}

fun resetLamp(): Int {
    return 0
}

fun resetIntensity(): Int {
    return 1
}