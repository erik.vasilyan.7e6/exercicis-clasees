/*
    * NAME: Erik Vasilyan
    * DATE: 2023-02-08
    * TITLE: MechanicalArmApp
 */

class MechanicalArm(var openAngle: Double = 0.0, var altitude: Double = 0.0, var turnedOn: Boolean = false) {

    fun toggle() {
        turnedOn = !turnedOn
    }

    fun updateAltitude(n: Int) {
        if (turnedOn) {
            altitude += n
            if (altitude + n > 30) altitude = 30.0
            else if (altitude + n < 0) altitude = 0.0
        }
    }

    fun updateAngle(n: Int) {
        if (turnedOn) {
            openAngle += n
            if (openAngle + n > 360) openAngle = 360.0
            else if (openAngle + n < 0) openAngle = 0.0
        }
    }
}

fun main() {
    val mechanicalArm = MechanicalArm()

    mechanicalArm.toggle()
    println("MechanicalArm{openAngle=${mechanicalArm.openAngle}, altitude=${mechanicalArm.altitude}, turnedOn=${mechanicalArm.turnedOn}}")
    mechanicalArm.updateAltitude(3)
    println("MechanicalArm{openAngle=${mechanicalArm.openAngle}, altitude=${mechanicalArm.altitude}, turnedOn=${mechanicalArm.turnedOn}}")
    mechanicalArm.updateAngle(180)
    println("MechanicalArm{openAngle=${mechanicalArm.openAngle}, altitude=${mechanicalArm.altitude}, turnedOn=${mechanicalArm.turnedOn}}")
    mechanicalArm.updateAltitude(-3)
    println("MechanicalArm{openAngle=${mechanicalArm.openAngle}, altitude=${mechanicalArm.altitude}, turnedOn=${mechanicalArm.turnedOn}}")
    mechanicalArm.updateAngle(-180)
    println("MechanicalArm{openAngle=${mechanicalArm.openAngle}, altitude=${mechanicalArm.altitude}, turnedOn=${mechanicalArm.turnedOn}}")
    mechanicalArm.updateAltitude(3)
    println("MechanicalArm{openAngle=${mechanicalArm.openAngle}, altitude=${mechanicalArm.altitude}, turnedOn=${mechanicalArm.turnedOn}}")
    mechanicalArm.toggle()
    println("MechanicalArm{openAngle=${mechanicalArm.openAngle}, altitude=${mechanicalArm.altitude}, turnedOn=${mechanicalArm.turnedOn}}")
}