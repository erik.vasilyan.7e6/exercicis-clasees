/*
    * NAME: Erik Vasilyan
    * DATE: 2023-02-03
    * TITLE: Pastisseria 2
 */

class Beguda (var nom: String, var preu: Double, var ensucrada: Boolean) {
    init {
        if (ensucrada) preu += (preu * 10) / 100
    }
}

fun main() {
    val croissant = Pasta("Croissant", 2.0, 50, 280)
    println("Nom: ${croissant.nom}")
    println("Preu: ${croissant.preu}€")
    println("Pes: ${croissant.pes}g")
    println("Calories: ${croissant.calories}kcal\n")

    val ensaimada = Pasta("Ensaimada", 10.0, 600, 2490)
    println("Nom: ${ensaimada.nom}")
    println("Preu: ${ensaimada.preu}€")
    println("Pes: ${ensaimada.pes}g")
    println("Calories: ${ensaimada.calories}kcal\n")

    val donut = Pasta("Donut", 1.0, 52, 209)
    println("Nom: ${donut.nom}")
    println("Preu: ${donut.preu}€")
    println("Pes: ${donut.pes}g")
    println("Calories: ${donut.calories}kcal\n")

    val aigua = Beguda("Aigua",1.0, false)
    println("Nom: ${aigua.nom}")
    println("Preu: ${aigua.preu}€")
    println("Increment: ${aigua.ensucrada}\n")

    val cafeTallat = Beguda("Cafe tallat", 1.35, false)
    println("Nom: ${cafeTallat.nom}")
    println("Preu: ${cafeTallat.preu}€")
    println("Increment: ${cafeTallat.ensucrada}\n")

    val teVermell = Beguda("Té vermell", 1.50, false)
    println("Nom: ${teVermell.nom}")
    println("Preu: ${teVermell.preu}€")
    println("Increment: ${teVermell.ensucrada}\n")

    val cola = Beguda("Cola", 1.65, true)
    println("Nom: ${cola.nom}")
    println("Preu: ${cola.preu}€")
    println("Increment: ${cola.ensucrada}")
}







