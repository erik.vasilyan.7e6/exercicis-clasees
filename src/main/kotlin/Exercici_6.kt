/*
    * NAME: Erik Vasilyan
    * DATE: 2023-02-08
    * TITLE: Electrodomèstics
 */

open class Electrodomestic(var preuBase: Int = 0, var color: String = "blanc", var consum: Char = 'G', var pes: Int = 5) {

    var preuFinal = 0

    init {
        preuFinal += preuBase + preuConsumIPes()
    }

    private fun preuConsumIPes(): Int {
        var preuConsum = 0
        when (consum) {
            'A' -> { preuConsum += 35 }
            'B' -> { preuConsum += 30 }
            'C' -> { preuConsum += 25 }
            'D' -> { preuConsum += 20 }
            'E' -> { preuConsum += 15 }
            'F' -> { preuConsum += 10 }
        }

        var preuPes = 0
        when (pes) {
            in 6..20 -> { preuPes += 20 }
            in 21..50 -> { preuPes += 50 }
            in 51..80 -> { preuPes += 80 }
        }
        if (pes > 80) preuPes += 100

        return preuConsum + preuPes
    }
}

class Rentadora(private var carrega: Int = 5, preuBase: Int, color: String, consum: Char, pes: Int): Electrodomestic(preuBase, color, consum, pes) {

    init {
        super.preuFinal += preuCarrega()
    }

    private fun preuCarrega(): Int {
        var preuCarrega = 0
        when (carrega) {
            6 or 7 -> { preuCarrega += 55 }
            8 -> { preuCarrega += 70 }
            9 -> { preuCarrega += 85 }
            10 -> { preuCarrega += 100 }
        }
        return preuCarrega
    }
}

class Televisio(private var tamany: Int = 28, preuBase: Int, color: String, consum: Char, pes: Int): Electrodomestic(preuBase, color, consum, pes) {

    init {
        super.preuFinal += preuTamany()
    }

    private fun preuTamany(): Int {
        var preuTamany = 0
        when (tamany) {
            in 29..32 -> { preuTamany += 50}
            in 33..42 -> { preuTamany += 100}
            in 43..50 -> { preuTamany += 150}
        }
        if (tamany > 50) preuTamany += 200
        return preuTamany
    }
}

fun main() {
    val electrodomestics = arrayOf(
        Electrodomestic(50, "negre", 'F', 15),
        Electrodomestic(65, "blau", 'G', 25),
        Electrodomestic(80, "taronja", 'B', 35),
        Electrodomestic(45, "vermell", 'C', 10),
        Electrodomestic(110, "blanc", 'A', 50),
        Electrodomestic(95, "lila", 'E', 45),
        Rentadora(5, 30, "blau", 'B', 70),
        Rentadora(8, 20, "blanc", 'A', 80),
        Televisio(52, 15, "negre", 'D', 20),
        Televisio(28, 45, "vermell", 'C', 15)
    )

    var electrodomesticsPreuBase = 0
    var electrodomesticsPreuFinal = 0
    var rentadoresPreuBase = 0
    var rentadoresPreuFinal = 0
    var televisionsPreuBase = 0
    var televisionsPreuFinal = 0

    var index = 1
    for (electrodomestic in electrodomestics) {
        println(
            "Electrodomèstic $index:" +
                    "\n   - Preu base: ${electrodomestic.preuBase}€" +
                    "\n   - Color: ${electrodomestic.color}" +
                    "\n   - Consum: ${electrodomestic.consum}" +
                    "\n   - Pes: ${electrodomestic.pes}kg" +
                    "\n   - Preu final: ${electrodomestic.preuFinal}€")

        when(electrodomestic.javaClass) {
            Electrodomestic::class.java -> {
                electrodomesticsPreuBase += electrodomestic.preuBase
                electrodomesticsPreuFinal += electrodomestic.preuFinal
            }
            Rentadora::class.java -> {
                rentadoresPreuBase += electrodomestic.preuBase
                rentadoresPreuFinal += electrodomestic.preuFinal
            }
            Televisio::class.java -> {
                televisionsPreuBase += electrodomestic.preuBase
                televisionsPreuFinal += electrodomestic.preuFinal
            }
        }
        index++
    }
    println("")

    println(
        "Electrodomèstics:" +
                "\n   - Preu base: $electrodomesticsPreuBase€" +
                "\n   - Preu final: $electrodomesticsPreuFinal€")

    println(
        "Rentadores:" +
                "\n   - Preu base: $rentadoresPreuBase€" +
                "\n   - Preu final: $rentadoresPreuFinal€")

    println(
        "Televisions:" +
                "\n   - Preu base: $televisionsPreuBase€" +
                "\n   - Preu final: $televisionsPreuFinal€")
}